<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Developed By Ahmed Feisal*/




//============================================================
Route::group(['middleware' => 'apilang'], function() {
    Route::get('relations','SkipController@relations');
    Route::post('father/register', 'AuthController@register');
    Route::post('father/login', 'AuthController@authenticate');
    Route::get('terms','SkipController@terms');
    Route::get('about','SkipController@about');
    Route::get('contact','SkipController@contact_us');
    Route::post('forget','AuthController@forget');
    Route::post('reset','AuthController@reset');

    Route::post('child/login', 'AuthController@authenticate_child');


Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('father/user', 'AuthController@getAuthenticatedUser');
    Route::post('father/change/password','AuthController@change_password');
    Route::post('father/update', 'AuthController@editUserProfile');
    Route::post('contact','SkipController@contact');
    Route::post('add/child','ChildController@add_child');
    Route::get('my/children','ChildController@my_children');
    Route::post('search/children','ChildController@search_children');
    Route::get('my/behaviors','PerantController@my_behaviors');
    Route::post('rate/child','PerantController@rate_child');
    Route::post('get/child','PerantController@get_child');
    Route::post('update/child','PerantController@update_child');
    Route::post('delete/child','PerantController@delete_child');
    Route::post('delete/child','PerantController@delete_child');
    Route::post('create/password/child','PerantController@create_password');
    Route::post('add/behavior','PerantController@add_behavior');
    Route::get('get/all/behaviors','PerantController@get_all_behaviors');
    Route::post('update/behavior','PerantController@update_behavior');
    Route::post('delete/behavior','PerantController@delete_behavior');
    Route::post('add/reward','PerantController@add_reward');
    Route::get('get/all/rewards','PerantController@get_all_rewards');
    Route::post('update/reward','PerantController@update_reward');
    Route::post('delete/reward','PerantController@delete_reward');
    Route::post('reward/get/children','PerantController@reward_get_children');
    Route::post('reward/assign/children','PerantController@reward_assign_children');
    Route::post('father/week/child','ChildController@week');
    Route::post('father/month/child','ChildController@month');
    Route::post('father/general/child','ChildController@general');
    Route::get('father/get/tips','TipController@get_parent');
    Route::post('father/store/tips','TipController@store');
    Route::post('father/update/tips','TipController@update');
    Route::post('father/delete/tips','TipController@delete');

    //======================= child =========================
    Route::get('child/home','ChildrenController@home');
    Route::get('child/my/rewards','ChildrenController@my_rewards');
    Route::get('child/week','ChildrenController@week');
    Route::get('child/month','ChildrenController@month');
    Route::get('child/general','ChildrenController@general');
    Route::get('child/tips','TipController@get_child');

    // ===================== common route
    Route::get('notifications','SkipController@notification');

});
});
