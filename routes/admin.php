<?php

//Route::get('/home', function () {
//
////    $school=auth()->user()->name;
////
////    $path=public_path('QR/');
////    $s='ssssssssss'.'.png';
////
////
////    \QrCode::format('png')->size(500)->generate('3', $path.$s);
//    return view('admin.home');
//})->name('home');

/* Developed By Ahmed Feisal*/

Route::get('/home','HomeController@index')->name('home');
Route::resource('settings','SettingController');
Route::resource('admins','AdminController');
Route::resource('relations','RelationController');
Route::resource('defaults','DefaultController');
Route::resource('parents','ParentController');
Route::resource('children','ChildConrtoller');
Route::resource('behaviors','BehaviorController');
Route::resource('rewards','RewardController');
Route::resource('tips','TipController');
Route::delete('child_behaviors/{id}','ChildConrtoller@behavior_destroy')->name('child_behaviors.destroy');
Route::delete('child_rewards/{id}','ChildConrtoller@reward_destroy')->name('child_rewards.destroy');
Route::get('child_set/{id}','ChildConrtoller@set_password')->name('children.set_password');

Route::get('send/notification',function(){
    return view('admin.notification.index');
})->name('notification.create');

Route::post('notification','NotificationController@notification')->name('notification.store');

