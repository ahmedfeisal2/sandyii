<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiMasterRequest;

class UpdateChild extends ApiMasterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'child_id'=>'required|exists:users,id',
            'name'=>'sometimes',
            'gender'=>'sometimes',
            'birthday'=>'sometimes',
            'image'=>'sometimes|image'
        ];
    }
}
