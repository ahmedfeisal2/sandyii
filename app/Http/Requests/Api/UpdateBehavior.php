<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiMasterRequest;

class UpdateBehavior extends ApiMasterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'behavior_id'=>'required|exists:behaviors,id',
            'title'=>'sometimes',
            'type'=>'sometimes|in:good,bad',
            'points'=>'sometimes|numeric'
        ];
    }
}
