<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiMasterRequest;

class UpdateTip extends ApiMasterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tip_id'=>'required|exists:tips,id',
            'title'=>'sometimes',
            'body'=>'sometimes'
        ];
    }
}
