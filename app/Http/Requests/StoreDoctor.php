<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDoctor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id=$this->route('doctor');
        return [
            'name'=>'required|string',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            'university_id'=>'required|numeric|exists:universities,id',
            'category_id'=>'required|numeric|exists:categories,id',
            'password' => 'required_without_all:_method|confirmed',
            'image'=>'required_without_all:_method|image',
            'gender'=>'required',
            'phone'=>'required_without_all:_method|unique:users,phone,'.$id,
            'age'=>'required|numeric',
            'year'=>'required|numeric',
            'grade'=>'required|string',
            'license'=>'required|string',
            'langs'=>'required|string',



        ];
    }
}
