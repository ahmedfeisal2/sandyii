<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$this->route('parent'),
            'password' => 'required_without_all:_method|confirmed',
            'image'=>'image',
            'mobile'=>'required|unique:users,mobile,'.$this->route('parent'),
            'gender'=>'required',
            'relation_id'=>'required|numeric|exists:relations,id',
        ];
    }
}
