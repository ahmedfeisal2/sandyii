<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RelationsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name='name_'.app()->getLocale();
        return $this->collection->transform(function ($query)use($name){
            return [
                'id'=>$query->id,
                'name'=>$query->$name,

            ];
        });
    }
}
