<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChildResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'gender'=>$this->gender,
            'birthday'=>$this->birthday,
            'image'=>asset($this->image),
            'points'=>$this->points,
            'identify_number'=>(string)$this->identify_number,
            'password'=>(string)$this->child_password,
            'age'=>\Carbon\Carbon::parse($this->birthday)->age,
            'behaviors_count'=>$this->childBehaviors->count(),
            'behaviors'=>$this->childBehaviors->transform(function ($query){
                    return [
                        'id'=>$query->behavior->id,
                        'title'=>$query->behavior->title,
                        'points'=>$query->behavior->points,
                        'type'=>$query->behavior->type,

                    ];

            }),
            'rewards'=>$this->childRewards->transform(function ($query){
                return [
                    'id'=>$query->reward->id,
                    'title'=>$query->reward->name,
                    'points'=>$query->reward->points,

                ];

            })


        ];
    }
}
