<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ChildenCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return $this->collection->transform(function ($query){
            return [
                'id'=>$query->id,
                'name'=>$query->name,
                'image'=>asset($query->image),
                'points'=>$query->points,
                'age'=>\Carbon\Carbon::parse($query->birthday)->age,
            ];
        });
    }
}
