<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ChildRewardCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($query){
            return [
                'id'=>$query->reward->id,
                'name'=>$query->reward->name,
                'points'=>$query->reward->points,
            ];
        });
    }
}
