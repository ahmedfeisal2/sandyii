<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use JWTAuth;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name='name_'.app()->getLocale();
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'GenderName'=>$this->genderName,
            'type'=>$this->type,
            'image'=> asset($this->image),
            'relation_id'=>$this->when(!is_null($this->relation_id),(int)$this->relation_id),
            'relation_name'=>!is_null($this->relation_id)?$this->relation->$name:'',
            'token'=>JWTAuth::fromUser($this),
        ];
    }
}
