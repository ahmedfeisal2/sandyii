<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChildProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'image'=>asset($this->image),
            'points'=>$this->points,
            'behavior_count'=>$this->childBehaviors->count(),
            'behaviors'=>$this->childBehaviors->transform(function ($query){
                return [
                    'id'=>$query->behavior->id,
                    'title'=>$query->behavior->title,
                    'points'=>$query->behavior->points,
                    'type'=>$query->behavior->type,
                    'date'=> $query->created_at->format('Y-m-d H:s a')
                ];

            }),
        ];
    }
}
