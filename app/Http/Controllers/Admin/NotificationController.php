<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class NotificationController extends Controller
{
    public function notification(Request $request){
        foreach (User::where('type','parent')->get()as $key){
            $this->createNotification('management',$request->title,$request->body,$key->id);
        }
            Flash::success('تم ارسال الاشعار بنجاح');

            return back();


    }
    protected function createNotification($type,$title,$message,$user_id)
    {

        Notification::create([
            'type' => $type,
            'user_id' => $user_id,
            'title' => $title,
            'body' => $message,
        ]);


    }
}
