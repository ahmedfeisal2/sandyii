<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ChildProfileResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\ChildRewardCollection;
use App\Models\Behavior;
use JWTAuth;

class ChildrenController extends Controller
{
    /* Developed By Ahmed Feisal*/

    public function home(){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        $data= new ChildProfileResource($user);
        return response()->json(['data'=>$data,'status'=>'ok'], 200);
    }

    public function my_rewards()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        $data= new ChildRewardCollection($user->childRewards);
        return response()->json(['data'=>$data,'status'=>'ok'], 200);
    }
    public function week(){
        if (! $child = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        $date=now();
        $start=$date->startOfWeek()->format('Y-m-d');
        $end=$date->endOfWeek()->format('Y-m-d');

        $total=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)->count();
        $bad_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
                $query->where('type','bad');
            })->count();
        $good_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
                $query->where('type','good');
            })->count();


        if ($total==0){
            $data['bad_count']=$data['good_count']=0.00;
        }else{
            $data['good_count']=(double)round(($good_count/$total)*100,2);
            $data['bad_count']=(double)round(($bad_count/$total)*100,2);
        }
        return response()->json(['data'=>$data,'status'=>'ok'], 200);

    }

    public function month(){
        if (! $child = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        $date=now();
        $start=$date->firstOfMonth()->format('Y-m-d');
        $end=$date->lastOfMonth()->format('Y-m-d');

        $total=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)->count();
        $bad_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
                $query->where('type','bad');
            })->count();
        $good_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
                $query->where('type','good');
            })->count();

        if ($total==0){
            $data['bad_count']=$data['good_count']=0.00;
        }else{
            $data['good_count']=(double)round(($good_count/$total)*100,2);
            $data['bad_count']=(double)round(($bad_count/$total)*100,2);
        }
        return response()->json(['data'=>$data,'status'=>'ok'], 200);

    }

    public function general(){
        if (! $child = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }


        $behaviors=$child->childBehaviors->groupBy('behavior_id');
        $data=[];
        foreach ($behaviors as $key=>$value){
            $behavior=Behavior::find($key);
            $data[] =[
                'key'=>$behavior->title ,
                'value'=>(double)round((count($value)/count($child->childBehaviors))*100,2)
            ];
        }

        return response()->json(['data'=>$data,'status'=>'ok'], 200);

    }
}
