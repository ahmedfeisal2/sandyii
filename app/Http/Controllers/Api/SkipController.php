<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ArticalResource;
use App\Http\Resources\FavoritesArticlesResource;
use App\Http\Resources\FavoritesDoctorsResource;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\RelationsCollection;
use App\Http\Resources\SingleArtical;
use App\Http\Resources\UniversitiesResource;
use App\Http\Resources\UserResource;
use App\Models\Artical;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Favorite;
use App\Models\Like;
use App\Models\Notification;
use App\Models\Relation;
use App\Models\University;
use Illuminate\Http\Request;
use App\Http\Requests\StoreContact;
use App\Http\Controllers\Controller;
use App\Http\Resources\IntrosResource;
use App\Http\Resources\CountriesResource;
use App\Http\Resources\CitiesResource;
use App\Http\Resources\TermsResource;
use App\Http\Resources\DoctorsResource;
use App\Http\Resources\LocationsResource;
use App\Models\Intro;
use App\Models\Country;
use App\Models\City;
use App\Models\Term;
use App\Models\Help;
use App\Models\Setting;
use App\Models\Contact;
use App\Models\Branche;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class SkipController extends Controller
{
    /* Developed By Ahmed Feisal*/

    public function relations(){

        $data=new RelationsCollection(Relation::all());
        return response()->json(['data'=>$data],200);

    }
    public function notification(){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        $notifications=Notification::where('user_id',$user->id)->get();
        $data=new NotificationCollection($notifications);
        return response()->json(['data'=>$data,'status'=>'ok'],200);
    }


    public function terms(){
        $terms=Setting::where('id',23)->select(['id','slug','ar_value as value'])->first();
        return response()->json(compact('terms'),200);
    }
    public function policy(){
        $policy=Setting::where('id',24)->select(['id','slug','ar_value as value'])->first();
        return response()->json(compact('policy'),200);
    }


    public function about(){
        $setting=Setting::where('id',26)->select(['id','slug',app()->getLocale().'_value as value'])->first();
        $status='ok';
        return response()->json(compact('setting','status'),200);
    }
    public function contact_us(){
        $setting['address']=Setting::where('id',24)->select(['id','slug',app()->getLocale().'_value as value'])->first()->value;
        $setting['email']=Setting::where('id',21)->select(['id','slug','ar_value as value'])->first()->value;
        $setting['website']=Setting::where('id',25)->select(['id','slug','ar_value as value'])->first()->value;
        $status='ok';
        return response()->json(compact('setting','status'),200);
    }

    public function contact(StoreContact $request){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $requests=$request->except('image');
        if ($request->hasFile('image')){
            $requests['image'] = saveImage($request->image, with(new Contact())->getTable());

        }
        $requests['user_id']=$user->id;

        $coun=Contact::create($requests);
        if (!$coun){

            return response()->json(['data'=>'حدث خطأ أثناء عملية الارسال'],201);

        }
        return response()->json(['data'=>'تم ارسال الرسالة بنجاح !'],200);

    }


}
