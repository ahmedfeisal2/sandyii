<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\DeleteTip;
use App\Http\Requests\Api\StoreTip;
use App\Http\Requests\Api\UpdateTip;
use App\Http\Resources\GeneralCollection;
use App\Http\Resources\TipResource;
use App\Models\Tip;
use App\Http\Controllers\Controller;
use JWTAuth;

class TipController extends Controller
{
    /* Developed By Ahmed Feisal*/
    protected $user;
    public function __construct()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $this->user=$user;
    }

    public function get_parent(){
        if ($this->user->type !='parent'){
            return response()->json(['message'=>'you not have permission to complete'], 405);
        }
        $user=$this->user;
        $tips=$user->tips;
        $data= new GeneralCollection($tips);
        return response()->json(['data'=>$data,'status'=>'ok'], 200);
    }
    public function get_child(){
        if ($this->user->type !='child'){
            return response()->json(['message'=>'you not have permission to complete'], 405);
        }
        $user=$this->user->parent;
        $tips=$user->tips;
        $data= new GeneralCollection($tips);
        return response()->json(['data'=>$data,'status'=>'ok'], 200);
    }
    public function store(StoreTip $request){
        if ($this->user->type !='parent'){
            return response()->json(['message'=>'you not have permission to complete'], 405);
        }
        $requests=$request->all();
        $requests['user_id']=$this->user->id;

        $tip=Tip::create($requests);
        $message='Tip created successfully ';
        if (app()->getLocale()=='ar'){
            $message='تم تسجيل النصحية بنجاح';
        }
        $data=new TipResource($tip);
        return response()->json(['data'=>$data ,'message'=>$message,'status'=>'ok'], 200);
    }
    public function update(UpdateTip $request){
        if ($this->user->type !='parent'){
            return response()->json(['message'=>'you not have permission to complete'], 405);
        }
        $requests=$request->all();
        $tip=Tip::find($request->tip_id);
        $tip->update($requests);

        $message='Tip Edited successfully ';
        if (app()->getLocale()=='ar'){
            $message='تم تعديل النصحية بنجاح';
        }
        $data=new TipResource($tip);
        return response()->json(['data'=>$data ,'message'=>$message,'status'=>'ok'], 200);
    }
    public function delete(DeleteTip $request){
        if ($this->user->type !='parent'){
            return response()->json(['message'=>'you not have permission to complete'], 405);
        }
        $tip=Tip::find($request->tip_id);
        $tip->delete();
        $message='Tip deleted Successfully';
        if (app()->getLocale()=='ar'){
            $message='تم حذف النصحية بنجاح';
        }
        return response()->json(['message'=>$message,'status'=>'ok'], 200);
    }
}
