<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\AddBehavior;
use App\Http\Requests\Api\AddReward;
use App\Http\Requests\Api\DeleteBehavior;
use App\Http\Requests\Api\DeleteChild;
use App\Http\Requests\Api\DeleteReward;
use App\Http\Requests\Api\GetChild;
use App\Http\Requests\Api\RateChild;
use App\Http\Requests\Api\RewardAssignChildren;
use App\Http\Requests\Api\RewardGetChildren;
use App\Http\Requests\Api\UpdateBehavior;
use App\Http\Requests\Api\UpdateChild;
use App\Http\Requests\Api\UpdateReward;
use App\Http\Resources\BehaviorResource;
use App\Http\Resources\BehaviorsCollection;
use App\Http\Controllers\Controller;
use App\Http\Resources\ChildenCollection;
use App\Http\Resources\ChildResource;
use App\Http\Resources\RewardCollection;
use App\Http\Resources\RewardResource;
use App\Models\Behavior;
use App\Models\Notification;
use App\Models\Reward;
use App\Models\UserBehavior;
use App\Models\UserReward;
use App\User;
use JWTAuth;
use Hash;

class PerantController extends Controller
{
    /* Developed By Ahmed Feisal*/

    public function my_behaviors(){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete'], 405);
        }
        $good = new BehaviorsCollection($user->behaviors()->findByType('good'));
        $bad = new BehaviorsCollection($user->behaviors()->findByType('bad')) ;

        return response()->json(['good'=>$good,'bad'=>$bad], 200);
    }

    public function rate_child(RateChild $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found'], 404);
        }
        $behavior_ids=$user->behaviors()->pluck('id')->toArray();
        $arr=array_diff($request->behavior_ids,$behavior_ids);
        if (count($arr)!=0){
            return response()->json(['data'=>$arr,'message'=>'these behaviors not found','status'=>'fails'], 404);
        }
        $good_count=0;
        $bad_count=0;
        $total=$child->points;
        foreach ($request->behavior_ids as $key){
            $behavior=Behavior::find($key);
            if ($behavior->type=='good'){
                $total+=$behavior->points;
                $good_count++;
            }else{
                $total-=$behavior->points;
                $bad_count++;
            }
            $user_behavior= new UserBehavior();
            $user_behavior->user_id=$request->child_id;
            $user_behavior->behavior_id=$key;
            $user_behavior->save();
        }
        $child->points=$total;
        $child->update();
        if ($good_count!=0){
            $this->createNotification('behavior','د اسناد سلوك ','تم اسناد '.$good_count.' سلوك جيد جديد لديك ',$request->child_id);
        }
        if ($bad_count!=0){
            $this->createNotification('behavior','اسناد سلوك','تم اسناد '.$good_count.' سلوك سيئ جديد لديك ',$request->child_id);
        }

        $message='Behaviors were assigned successfully ';
        if (app()->getLocale()=='ar'){
            $message='تم اسناد السلوكيات بنجاح';
        }
        return response()->json(['message'=>$message,'status'=>'ok'], 200);

    }

    public function get_child(GetChild $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found'], 404);
        }
        $data=new ChildResource($child);
        return response()->json(['data'=>$data,'status'=>'ok'], 200);
    }

    public function update_child(UpdateChild $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found','status'=>'fails'], 404);
        }
        if ($request->name){
            $child->name=$request->name;
        }
        if ($request->gender){
            $child->gender=$request->gender;
        }
        if ($request->birthday){
            $child->birthday=$request->birthday;
        }
        if ($request->image){
            $child->image = saveImage($request->image, with(new User)->getTable());
        }
        $child->update();
        $data=new ChildResource($child);
        $message='Child updated Successfuly';
        if (app()->getLocale()=='ar'){
            $message='تم تعديل البيانات بنجاح';
        }
        return response()->json(['data'=>$data,'message'=>$message,'status'=>'ok'], 200);
    }

    public function delete_child(DeleteChild $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found','status'=>'fails'], 404);
        }
        $child->delete();
        $message='Child deleted Successfuly';
        if (app()->getLocale()=='ar'){
            $message='تم حذف الطفل بنجاح';
        }
        return response()->json(['message'=>$message,'status'=>'ok'], 200);
    }

    public function create_password(GetChild $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found','status'=>'fails'], 404);
        }
        $password=mt_rand(1000, 9999);
        $child->child_password=$password;
        $child->identify_number=mt_rand(10000000, 99999999);
        $child->password = Hash::make($password);
        $child->update();

        $message='Password created Successfuly';
        if (app()->getLocale()=='ar'){
            $message='تم انشاء كلمة المرور بنجاح';
        }
        $data['identify_number']=$child->identify_number;
        $data['password']=$password;
        return response()->json(['data'=>$data,'message'=>$message,'status'=>'ok'], 200);
    }

    public function add_behavior(AddBehavior $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $behavior=new Behavior();
        $behavior->title=$request->title;
        $behavior->type=$request->type;
        $behavior->points=$request->points;
        $behavior->user_id=$user->id;
        $behavior->save();
        $message='Behavior wa added successfully';
        if (app()->getLocale()=='ar'){
            $message='تم اضافة السلوك بنجاح';
        }

        return response()->json(['data'=>new BehaviorResource($behavior),'message'=>$message,'status'=>'ok'], 200);

    }

    public function get_all_behaviors(){

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        return response()->json(['data'=>new BehaviorsCollection($user->behaviors),'status'=>'ok'], 200);

    }

    public function update_behavior(UpdateBehavior $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $behavior=$user->behaviors()->where('id',$request->behavior_id)->first();
        if (is_null($behavior)){
            return response()->json(['message'=>'Behavior not found','status'=>'fails'], 404);
        }
        if ($request->title){
            $behavior->title=$request->title;
        }
        if ($request->type){
            $behavior->type=$request->type;
        }
        if ($request->points){
            $behavior->points=$request->points;
        }
        $behavior->update();
        $message='update successful';
        if (app()->getLocale()=='ar'){
            $message='تم التعديل بنجاح';
        }
        return response()->json(['data'=>new BehaviorResource($behavior),'message'=>$message,'status'=>'ok'], 200);

    }

    public function delete_behavior(DeleteBehavior $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $behavior=$user->behaviors()->where('id',$request->behavior_id)->first();
        if (is_null($behavior)){
            return response()->json(['message'=>'Behavior not found','status'=>'fails'], 404);
        }
        $behavior->delete();
        $message='deleted successfully';
        if (app()->getLocale()=='ar'){
            $message='تم الحذف بنجاح';
        }
        return response()->json(['message'=>$message,'status'=>'ok'], 200);
    }

    public function add_reward(AddReward $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $requests=$request->all();
        $requests['user_id']=$user->id;
        $reward=Reward::create($requests);
        $message='Reward wa added successfully';
        if (app()->getLocale()=='ar'){
            $message='تم اضافة المكافأة بنجاح';
        }

        return response()->json(['data'=>new RewardResource($reward),'message'=>$message,'status'=>'ok'], 200);

    }

    public function update_reward(UpdateReward $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $reward=$user->rewards()->where('id',$request->reward_id)->first();
        if (is_null($reward)){
            return response()->json(['message'=>'Reward not found','status'=>'fails'], 404);
        }
        if ($request->name){
            $reward->name=$request->name;
        }

        if ($request->points){
            $reward->points=$request->points;
        }
        $reward->update();
        $message='update successful';
        if (app()->getLocale()=='ar'){
            $message='تم التعديل بنجاح';
        }
        return response()->json(['data'=>new RewardResource($reward),'message'=>$message,'status'=>'ok'], 200);

    }

    public function get_all_rewards(){

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        return response()->json(['data'=>new RewardCollection($user->rewards),'status'=>'ok'], 200);

    }

    public function delete_reward(DeleteReward $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $reward=$user->rewards()->where('id',$request->reward_id)->first();
        if (is_null($reward)){
            return response()->json(['message'=>'Reward not found','status'=>'fails'], 404);
        }
        $reward->delete();
        $message='deleted successfully';
        if (app()->getLocale()=='ar'){
            $message='تم الحذف بنجاح';
        }
        return response()->json(['message'=>$message,'status'=>'ok'], 200);
    }

    public function reward_get_children(RewardGetChildren $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $reward=$user->rewards()->where('id',$request->reward_id)->first();
        if (is_null($reward)){
            return response()->json(['message'=>'Reward not found','status'=>'fails'], 404);
        }
        $children=$user->children()->where('points','>=',$reward->points)->get();

        return response()->json(['data'=>new ChildenCollection($children),'status'=>'ok'], 200);
    }

    public function reward_assign_children(RewardAssignChildren $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $reward=$user->rewards()->where('id',$request->reward_id)->first();
        if (is_null($reward)){
            return response()->json(['message'=>'Reward not found','status'=>'fails'], 404);
        }
        $children_ids=$user->children()->pluck('id')->toArray();
        $arr=array_diff($request->children_ids,$children_ids);
        if (count($arr)!=0){
            return response()->json(['data'=>$arr,'message'=>'these children not found','status'=>'fails'], 404);
        }

        foreach ($request->children_ids as $key){
            $child=User::find($key);
            $user_reward= new UserReward();
            $user_reward->user_id=$key;
            $user_reward->reward_id=$reward->id;
            $user_reward->save();
            $child->points -=$reward->points;
            $child->update();
            $this->createNotification('reward','اضافة مكافأة','تم اضافة مكافأة لك ',$key);
        }


        $message='Reward were assigned successfully ';
        if (app()->getLocale()=='ar'){
            $message='تم اسناد المكافاة بنجاح';
        }
        return response()->json(['message'=>$message,'status'=>'ok'], 200);

    }

    protected function createNotification($type,$title,$message,$user_id)
    {

        Notification::create([
            'type' => $type,
            'user_id' => $user_id,
            'title' => $title,
            'body' => $message,
        ]);


    }
}
