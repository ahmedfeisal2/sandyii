<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ChangePassword;
use App\Http\Requests\Api\ChildLogin;
use App\Http\Requests\Api\ForgetRequest;
use App\Http\Requests\Api\ResetRequest;
use App\Mail\ResetMail;
use App\Models\Behavior;
use App\Models\Defaults;
use App\Models\Notification;
use App\Models\Provider;
use App\User;
use App\Models\Device;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Http\Requests\Api\UpdateProfile;
use App\Http\Requests\Api\StoreRegister;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Resources\UserResource;

class AuthController extends Controller
{
    /* Developed By Ahmed Feisal*/

    public function authenticate(LoginRequest $request)
    {

        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'من فضلك تأكد من الايميل و كلمة المرور','status'=>'fails'], 400);
            }
            $user = User::whereEmail($request->email)->first();

            if (count($user->devices->where('device_token',$request->device_token))==0){

                $dev['user_id']=$user->id;
                $dev['device_token']=$request->device_token;
                $dev['device_type']=$request->device_type;
                $device=Device::create($dev);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'could_not_create_token','status'=>'fails'], 500);
        }


        $user= new UserResource($user);
        return response()->json(['data'=>$user,'status'=>'ok']);
    }

    public function register(StoreRegister $request)
    {
        $requests = $request->all();
        $requests['password'] = Hash::make($request->password);
        if ($request->hasFile('image')) {

            $requests['image'] = saveImage($request->image, with(new User)->getTable());
        }
        $requests['type']='parent';

        $user= User::create($requests);
//        if ($request->has(['provider_id','provider'])){
//            $provider=Provider::create(['provider_id'=>$request->provider_id,'provider'=>$request->provider]);
//        }
//        $data=$user;
//        $token = JWTAuth::fromUser($user);
        if (count($user->devices->where('device_token',$request->device_token))==0){

            $dev['user_id']=$user->id;
            $dev['device_token']=$request->device_token;
            $dev['device_type']=$request->device_type;
            $device=Device::create($dev);
        }

        foreach (Defaults::all() as $key){
            $defaults['title']=$key->title;
            $defaults['type']=$key->type;
            $defaults['points']=$key->points;
            $defaults['user_id']=$user->id;
            Behavior::create($defaults);
        }

        $data= new UserResource($user);
//        $data['token']=$token;

        return response()->json(['data'=>$data,'status'=>'ok']);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $data= new UserResource($user);
        return response()->json(['data'=>$data,'status'=>'ok']);
    }

    public function editUserProfile(UpdateProfile $request){

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['هذا المستخدم غير موجود'], 404);
        }


        $requests=$request->except('password','image');


        if (!is_null($request->has('password'))) {
            $requests['password'] = Hash::make($request->password);
        }
        if ($request->hasFile('image')) {

            $requests['image'] = saveImage($request->image, with(new User)->getTable());
        }

        $user->update($requests);
        $user= new UserResource($user);
        return response()->json(['message'=>'تم التعديل بنجاح','data'=>$user,'status'=>'ok'], 200);

    }

    public function change_password(ChangePassword $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        if (!Hash::check($request->old_password, $user->password)) {
            $message='من فضلك تأكد من كلمة المرور القديمة';
            if (app()->getLocale()=='en'){
                $message='old password is invalid';
            }
            return response()->json(['message' => $message], 400);

        }
        $requests['password'] = Hash::make($request->new_password);
        $user->update($requests);

        $data= new UserResource($user);
        return response()->json(['data'=>$data,'status'=>'ok'],200);
    }

    public function forget(ForgetRequest $request){
        $user=User::whereEmail($request->email)->first();
        if (is_null($user)){
            return response()->json(['message'=>'هذا الايميل غير موجود !'], 405);

        }
        $reset=mt_rand(1000, 9999);
        $user->reset_code=$reset;
        $user->save();
        $mail = \Mail::to($user->email)->send(new ResetMail($user));

        return response()->json(['message'=>'تم ارسال كود الاسترجاع بنجاح !' ,'code'=>$reset,'status'=>'ok'], 200);
    }

    public function reset(ResetRequest $request){
        $user=User::where('reset_code',$request->code)->first();
        if (is_null($user)){
            return response()->json(['message'=>'هذا الكود غير موجود !'], 405);

        }
        $user->password = Hash::make($request->password);
        $user->reset_code=null;
        $user->update();
        $user= new UserResource($user);
        return response()->json(['message'=>'تم التعديل بنجاح','data'=>$user,'status'=>'ok']);

    }

    public function authenticate_child(ChildLogin $request)
    {

        $credentials = $request->only('identify_number', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'من فضلك تأكد من الرقم التعريفي و كلمة المرور','status'=>'fails'], 400);
            }
            $user = User::where('identify_number',$request->identify_number)->first();

            if (count($user->devices->where('device_token',$request->device_token))==0){

                $dev['user_id']=$user->id;
                $dev['device_token']=$request->device_token;
                $dev['device_type']=$request->device_type;
                $device=Device::create($dev);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'could_not_create_token','status'=>'fails'], 500);
        }


        $user= new UserResource($user);
        return response()->json(['data'=>$user,'status'=>'ok']);
    }

}
