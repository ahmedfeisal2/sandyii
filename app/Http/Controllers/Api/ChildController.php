<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\AddChild;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SearchChildren;
use App\Http\Requests\Api\WeekRequest;
use App\Http\Resources\ChildenCollection;
use App\Models\Behavior;
use App\Models\UserBehavior;
use App\User;
use JWTAuth;

class ChildController extends Controller
{
    /* Developed By Ahmed Feisal*/

    public function add_child(AddChild $request){

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child= new User();
        $child->name=$request->name;
        $child->gender=$request->gender;
        $child->birthday=$request->birthday;
        $child->parent_id=$user->id;
        $child->type='child';
        if ($request->image){
            $child->image = saveImage($request->image, with(new User)->getTable());
        }
        $child->save();
        $message='created successfully ';
        if (app()->getLocale()=='ar'){
            $message='تم بنجاح';
        }
        return response()->json(['message'=>$message,'status'=>'ok'], 200);
    }

    public function my_children(){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }

        $data=new ChildenCollection($user->children);
        return response()->json(['data'=>$data,'status'=>'ok'], 200);
    }

    public function search_children(SearchChildren $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $children=$user->children()->where('name','like','%'.$request->search.'%')->get();
        $data=new ChildenCollection($children);
        return response()->json(['data'=>$data,'status'=>'ok'], 200);
    }

    public function week(WeekRequest $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found'], 404);
        }
        $date=now();
        $start=$date->startOfWeek()->format('Y-m-d');
        $end=$date->endOfWeek()->format('Y-m-d');

        $total=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)->count();
        $bad_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
                $query->where('type','bad');
            })->count();
        $good_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
            $query->where('type','good');
        })->count();
        if ($total==0){
            $data['bad_count']=$data['good_count']=0.00;
        }else{
            $data['good_count']=(double)round(($good_count/$total)*100,2);
            $data['bad_count']=(double)round(($bad_count/$total)*100,2);
        }

        return response()->json(['data'=>$data,'status'=>'ok'], 200);

    }

    public function month(WeekRequest $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found'], 404);
        }
        $date=now();
        $start=$date->firstOfMonth()->format('Y-m-d');
        $end=$date->lastOfMonth()->format('Y-m-d');

        $total=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)->count();
        $bad_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
                $query->where('type','bad');
            })->count();
        $good_count=$child->childBehaviors()->where('created_at','>=',$start)->where('created_at','<=',$end)
            ->whereHas('behavior',function ($query){
            $query->where('type','good');
        })->count();

        if ($total==0){
            $data['bad_count']=$data['good_count']=0.00;
        }else{
            $data['good_count']=(double)round(($good_count/$total)*100,2);
            $data['bad_count']=(double)round(($bad_count/$total)*100,2);
        }
        return response()->json(['data'=>$data,'status'=>'ok'], 200);

    }

    public function general(WeekRequest $request){
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if ($user->type!='parent'){
            return response()->json(['message'=>'you not have permission to complete','status'=>'fails'], 405);
        }
        $child=$user->children()->where('id',$request->child_id)->first();
        if (is_null($child)){
            return response()->json(['message'=>'child not found'], 404);
        }

        $behaviors=$child->childBehaviors->groupBy('behavior_id');
        $data=[];
        foreach ($behaviors as $key=>$value){
            $behavior=Behavior::find($key);
            $data[] =[
                'key'=>$behavior->title ,
                'value'=>(double)round((count($value)/count($child->childBehaviors))*100,2)
            ];
        }

        return response()->json(['data'=>$data,'status'=>'ok'], 200);

    }
}
