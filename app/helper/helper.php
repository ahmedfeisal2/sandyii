<?php
/* Developed By Ahmed Feisal*/

function saveImage($file, $folder)
{

    $fileName = rand(99999, 99999999) .$file->getClientOriginalName();

    $dest = public_path($folder);
    $file->move($dest, $fileName);

    return  'public/'. $folder . '/' . $fileName;
}

function showImage($image, $alt = null, $option = [])
{
    return Html::image(url($image), $alt, $option);
}

function relations(){
    $university=[];
    foreach (App\Models\Relation::all() as  $key){
        $university[$key->id]=$key->name_ar;
    }
    return $university;
}
function parents(){
    $university=[];
    foreach (App\User::where('type','parent')->get() as  $key){
        $university[$key->id]=$key->name;
    }
    return $university;
}

function years(){
    $current_year = date('Y');
    $range = range($current_year, $current_year+10);
    $years = array_combine($range, $range);
    return $years;
}
function categories(){
    $categories=[];
    foreach (App\Models\Category::all() as  $key){
        $categories[$key->id]=$key->name;
    }
    return $categories;
}



function set_active($path, $active = 'active') {


    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

function send_notification($token, $msg)
{
    $api='AAAAuBpi290:APA91bGKi4uTHDbj69LgewuV07jNd7F1Iy65jsa4d8c3on4SsOBu7wWwb6BVdiUo4pWcl2VQpRK66kZJZ4-blus9yGiU7SXtG2UozSyjt6YaDdFYfz53eV_76OEcfdyu4ia52KixJ7C4GPcCotuyZM-ncPVru5dufQ';

    #prep the bundle
    $data = $msg ; // ['key' => 'value']; // data to send
#prep the bundle
    $fields = array
    (
        'to' => $token, //AllDevices
        'data' => $data,
    );



    $headers = array
    (
        'Authorization: key='. $api,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    curl_close( $ch );
    return json_decode($result);

}



function sendFCM($token, $data,$type)
{
    $optionBuilder = new \LaravelFCM\Message\OptionsBuilder();
    $optionBuilder->setTimeToLive(60 * 20);

    $notificationBuilder = new \LaravelFCM\Message\PayloadNotificationBuilder('the Title from notificationObject');//Title
    $notificationBuilder->setBody('the Body of the notificationObject')//Body
    ->setSound('default');

    $dataBuilder = new \LaravelFCM\Message\PayloadDataBuilder();
//    $dataBuilder->setData($data);
    $dataBuilder->addData($data);

    $option = $optionBuilder->build();
    $notification = $notificationBuilder->build();
    $data = $dataBuilder->build();

    //$token = '';
    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
    //$downstreamResponse->numberFailure();
}

function sendTopic($data){
    $notificationBuilder = new \LaravelFCM\Message\PayloadNotificationBuilder('my title');
    $notificationBuilder->setBody('Hello world')
        ->setSound('default');

    $notification = $notificationBuilder->build();
    $dataBuilder = new \LaravelFCM\Message\PayloadDataBuilder();
    $dataBuilder->setData($data);
    $data = $dataBuilder->build();
    $topic = new \LaravelFCM\Message\Topics();
    $topic->topic('status');

    $topicResponse = FCM::sendToTopic($topic, null, $notification, $data);

    $topicResponse->isSuccess();
    $topicResponse->shouldRetry();
    $topicResponse->error();
}
