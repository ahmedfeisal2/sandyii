<?php

namespace App;

use App\Models\Behavior;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\Relation;
use App\Models\Reward;
use App\Models\Tip;
use App\Models\University;
use App\Models\UserBehavior;
use App\Models\UserReward;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Category;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //unavailable
    protected $fillable = [
        'name','gender','image', 'email', 'password','relation_id','parent_id','type','birthday',
        'identify_number','reset_code','mobile','points','child_password'
    ];
    protected $appends=['GenderName'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];


    public function scopefindByType($query, $type)
    {
        return $query->where('type', $type)->get();
    }


    public function getGenderNameAttribute(){
        switch ($this->gender) {
            case 'male':
                if (app()->getLocale()=='en'){
                    return 'Male';
                }
                return 'ذكر';
        break;
            case 'female':
                if (app()->getLocale()=='en'){
                    return 'Female';
                }
                return 'أنثي';
        break;
        case 'other':
                if (app()->getLocale()=='en'){
                    return 'Other';
                }
                return 'أخري';
        break;

        }

    }

    public function relation(){
        return $this->belongsTo(Relation::class,'relation_id');
    }
    public function parent(){
        return $this->belongsTo(User::class,'parent_id');
    }

    public function children(){
        return $this->hasMany(User::class,'parent_id');
    }

    public function devices(){
        return $this->hasMany('App\Models\Device','user_id','id');
    }
    public function behaviors(){
        return $this->hasMany(Behavior::class,'user_id');
    }
    public function rewards(){
        return $this->hasMany(Reward::class,'user_id');
    }
    public function childBehaviors(){
        return $this->hasMany(UserBehavior::class,'user_id');
    }
    public function childRewards(){
        return $this->hasMany(UserReward::class,'user_id');
    }
    public function tips(){
        return $this->hasMany(Tip::class,'user_id');
    }







}
