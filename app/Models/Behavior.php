<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Behavior extends Model
{
    protected $table='behaviors';
    protected $fillable = ['type','title','user_id','points'];

    public function scopefindByType($query, $type)
    {
        return $query->where('type', $type)->get();
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
