<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserReward extends Model
{
    public function reward(){
        return $this->belongsTo(Reward::class,'reward_id');
    }
}
