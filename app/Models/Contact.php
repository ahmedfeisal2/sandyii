<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table='contacts';
    protected $fillable=['user_id','message','subject','image','reply'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
