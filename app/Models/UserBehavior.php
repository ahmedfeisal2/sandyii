<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBehavior extends Model
{
    public function behavior(){
        return $this->belongsTo(Behavior::class,'behavior_id');
    }
}
