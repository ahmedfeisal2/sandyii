<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $fillable=['name','points','user_id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
