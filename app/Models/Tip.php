<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    protected $table='tips';
    protected $fillable=['user_id','title','body'];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
