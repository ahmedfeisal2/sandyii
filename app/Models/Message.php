<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table='messages';
    protected $fillable=['sender_id','receiver_id','conversation_id','type','message'];
    public function sender(){
        return $this->belongsTo(User::class,'sender_id','id');
    }
    public function receiver(){
        return $this->belongsTo(User::class,'receiver_id','id');

    }
    public function conversation(){
        return $this->belongsTo(Conversation::class,'conversation_id','id');

    }
}
