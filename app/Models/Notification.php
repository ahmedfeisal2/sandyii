<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table='notification';
    protected $fillable = [
        'type',
        'user_id',
        'title',
        'body',
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
