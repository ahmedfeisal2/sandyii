<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAq6XKqQI:APA91bG8azAgXa4QgqbpNPWSIkDwWa9DE6Rw8xwE0MuT1ZilL51bYI0msAjBdOXUlfbOrQjBqYmh1rXimPvvmXtGRSdBpXkNGCxTEId0ij9s4-it8khS9_q6O_RwKbovZ5eQs97MaqZf'),
        'sender_id' => env('FCM_SENDER_ID', '737220929794'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
