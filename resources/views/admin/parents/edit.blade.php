@extends('admin.layouts.app')
@section('title')تعديل بيانات  {{$parent->name}}@endsection
@section('header')@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-pin font-green"></i>
                        <span class="caption-subject bold uppercase">تعديل بيانات  {{$parent->name}}</span>
                    </div>

                </div>
                <div class="portlet-body form">

                    {!!Form::model($parent,['route'=>['parents.update',$parent->id],'method'=>'put', 'novalidate','files'=>'true'])!!}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @include('admin.parents._form')
                    {{Form::close()}}
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
            <!-- BEGIN SAMPLE FORM PORTLET-->

            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
@endsection
@section('footer')


@endsection




