<table class="table table-striped table-bordered  display example table-hover dt-responsive" width="100%"
       id="sample_3" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="all  text-center">#</th>
        <th class="all  text-center">الصورة</th>
        <th class="all  text-center">الاسم</th>
        <th class="all  text-center">البريد الالكتروني</th>
        <th class="all  text-center">صلة القرابة</th>
        <th class="all  text-center">الجنس</th>
        <th class="min-tablet text-center">الاعدادت</th>

    </tr>
    </thead>
    <tbody>
    @foreach($parents as $parent)
        <tr>
            <td class="text-center">{!!$loop->iteration!!}</td>
            <td><img src="{!! asset($parent->image) !!}" width="100px" ></td>
            <td>{!!$parent->name!!}</td>
            <td>{!!$parent->email!!}</td>
            <td>{!! ! is_null($parent->relation)?$parent->relation->name_ar:'---'!!}</td>
            <td>{!!$parent->gender=='male'?'ذكر':'أنثي'!!}</td>

            {!!Form::open( ['route' => ['parents.destroy',$parent->id] ,'id'=>'delete-form'.$parent->id, 'method' => 'Delete']) !!}
            {!!Form::close() !!}
            <td colspan="1">
                <div class="margin-bottom-5">
                    <a href="{{route('parents.edit',[$parent->id])}}"
                       class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil-square"></i> تعديل </a>
                </div>

                <div class="margin-bottom-5">
                    <a onclick="Delete({{$parent->id}})" href="#"
                       class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                        <i class="fa fa-trash"></i> حذف</a>
                </div>
                <div class="margin-bottom-5">
                    <a href="{{route('parents.show',[$parent->id])}}"
                       class="btn btn-sm blue btn-outline filter-submit margin-bottom">
                        <i class="fa fa-eye"></i> عرض</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
