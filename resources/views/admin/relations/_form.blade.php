<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('name_ar') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('name_ar',old('name_ar'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">الاسم بالعربية</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('name_en') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('name_en',old('name_en'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">الاسم بالانجليزية</label>
    </div>

</div>
<div class="form-actions noborder">
    <button type="submit" class="btn blue btn_1">حفظ</button>
    <button type="reset" class="btn default btn_2">الغاء</button>
</div>
