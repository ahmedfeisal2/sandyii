<table class="table datatable-button-init-basic display example">
    <thead>
    <tr>
        <th> # </th>
        <th> الاسم بالعربية </th>
        <th> الاسم بالانجليزية </th>
        <th> العمليات </th>
    </tr>
    </thead>
    <tbody>
    @foreach($relations as $key)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td>{{$key->name_ar}}</td>
            <td>{{$key->name_en}}</td>
            {!!Form::open( ['route' => ['relations.destroy',$key->id] ,'id'=>'delete-form'.$key->id, 'method' => 'Delete']) !!}
            {!!Form::close() !!}
            <td>
                <div class="margin-bottom-5">
                    <a onclick="Delete({{$key->id}})" href="#"
                       class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                        <i class="fa fa-trash"></i> حذف</a>
                </div>

                <div class="margin-bottom-5">
                    <a href="{{route('relations.edit',['id'=>$key->id])}}"
                       class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil-square"></i> تعديل</a>
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

