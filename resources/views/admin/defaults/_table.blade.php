<table class="table datatable-button-init-basic display example">
    <thead>
    <tr>
        <th> # </th>
        <th> الاسم  </th>
        <th> النقاط </th>
        <th> النوع </th>
        <th> العمليات </th>
    </tr>
    </thead>
    <tbody>
    @foreach($defaults as $key)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td>{{$key->title}}</td>
            <td>{{$key->type=='good'?$key->points:'-'.$key->points}}</td>
            <td>{{$key->type=='good'?'جيد':'سيئ'}}</td>
            {!!Form::open( ['route' => ['defaults.destroy',$key->id] ,'id'=>'delete-form'.$key->id, 'method' => 'Delete']) !!}
            {!!Form::close() !!}
            <td>
                <div class="margin-bottom-5">
                    <a onclick="Delete({{$key->id}})" href="#"
                       class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                        <i class="fa fa-trash"></i> حذف</a>
                </div>

                <div class="margin-bottom-5">
                    <a href="{{route('defaults.edit',['id'=>$key->id])}}"
                       class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil-square"></i> تعديل</a>
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

