<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('title') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('title',old('title'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">الاسم </label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('points') ? ' has-error' : 'has-success' }} ">
        {!!Form::number('points',old('points'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">النقاط</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('type') ? ' has-error' : 'has-success' }} ">
        {!!Form::select('type',['good'=>'جيد','bad'=>'سيئ'],old('type'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">النوع</label>
    </div>

</div>
<div class="form-actions noborder">
    <button type="submit" class="btn blue btn_1">حفظ</button>
    <button type="reset" class="btn default btn_2">الغاء</button>
</div>
