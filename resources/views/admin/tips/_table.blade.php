<table class="table datatable-button-init-basic display example">
    <thead>
    <tr>
        <th> # </th>
        <th> ولي الامر </th>
        <th> العنوان </th>
        <th> المحتوي </th>
        <th> العمليات </th>
    </tr>
    </thead>
    <tbody>
    @foreach($tips as $key)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td><a href="{{route('parents.show',$key->user_id)}}" target="_blank">{{ $key->user->name }}</a></td>
            <td>{{$key->title}}</td>
            <td>{{$key->body}}</td>
            {!!Form::open( ['route' => ['tips.destroy',$key->id] ,'id'=>'delete-form'.$key->id, 'method' => 'Delete']) !!}
            {!!Form::close() !!}
            <td>
                <div class="margin-bottom-5">
                    <a onclick="Delete({{$key->id}})" href="#"
                       class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                        <i class="fa fa-trash"></i> حذف</a>
                </div>

                <div class="margin-bottom-5">
                    <a href="{{route('tips.edit',['id'=>$key->id])}}"
                       class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil-square"></i> تعديل</a>
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

