<div class="form-body">


        <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('user_id') ? ' has-error' : 'has-success' }} ">
            {!!Form::select('user_id',parents(),old('user_id'),['class'=>'form-control','required'=>'required'])!!}
            <label for="form_control_1">ولي الأمر</label>
        </div>

    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('title') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('title',old('title'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">العنوان</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('body') ? ' has-error' : 'has-success' }} ">
        {!!Form::textarea('body',old('body'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">المحتوي</label>
    </div>

</div>
<div class="form-actions noborder">
    <button type="submit" class="btn blue btn_1">حفظ</button>
    <button type="reset" class="btn default btn_2">الغاء</button>
</div>
