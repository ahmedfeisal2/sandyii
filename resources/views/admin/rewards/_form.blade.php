<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('user_id') ? ' has-error' : 'has-success' }} ">
        {!!Form::select('user_id',parents(),old('user_id'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">ولي الأمر</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('name') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('name',old('name'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">الاسم </label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('points') ? ' has-error' : 'has-success' }} ">
        {!!Form::number('points',old('points'),['class'=>'form-control','required'=>'required'])!!}
        <label for="form_control_1">النقاط</label>
    </div>

</div>
<div class="form-actions noborder">
    <button type="submit" class="btn blue btn_1">حفظ</button>
    <button type="reset" class="btn default btn_2">الغاء</button>
</div>
