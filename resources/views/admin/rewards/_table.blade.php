<table class="table datatable-button-init-basic display example">
    <thead>
    <tr>
        <th> # </th>
        <th> الاسم  </th>
        <th> النقاط </th>
        <th> ولي الامر </th>
        <th> العمليات </th>
    </tr>
    </thead>
    <tbody>
    @foreach($defaults as $key)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td>{{$key->name}}</td>
            <td>{{$key->points}}</td>
            <td><a href="{{route('rewards.show',[$key->user->id])}}">{{$key->user->name}}</a></td>
            {!!Form::open( ['route' => ['rewards.destroy',$key->id] ,'id'=>'delete-form'.$key->id, 'method' => 'Delete']) !!}
            {!!Form::close() !!}
            <td>
                <div class="margin-bottom-5">
                    <a onclick="Delete({{$key->id}})" href="#"
                       class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                        <i class="fa fa-trash"></i> حذف</a>
                </div>

                <div class="margin-bottom-5">
                    <a href="{{route('rewards.edit',['id'=>$key->id])}}"
                       class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil-square"></i> تعديل</a>
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

