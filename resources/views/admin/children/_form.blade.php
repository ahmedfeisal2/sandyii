<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('name') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('name',old('name'),['class'=>'form-control'])!!}
        <label for="form_control_1">الاسم</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('birthday') ? ' has-error' : 'has-success' }} ">
        {!!Form::date('birthday',old('birthday'),['class'=>'form-control'])!!}
        <label for="form_control_1"> تاريخ الميلاد</label>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('gender') ? ' has-error' : 'has-success' }} ">
        {!!Form::select('gender',['male'=>'ذكر','female'=>'أنثي'],old('gender'),['class'=>'form-control'])!!}
        <label for="form_control_1">الجنس</label>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label{{ $errors->has('image') ? ' has-error' : 'has-success' }}">
        @if(isset($child))
            <img src="{!! asset($child->image) !!}" width="100px">
        @endif
        {!!Form::file('image',['class'=>'form-control'])!!}
        <label for="form_control_1">الصوره</label>
    </div>

</div>
<div class="form-actions noborder">
    <button type="submit" class="btn blue btn_1">حفظ</button>
    <button type="reset" class="btn default btn_2">الغاء</button>
</div>
