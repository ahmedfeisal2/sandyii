@extends('admin.layouts.app')
@section('title')  {{$child->name}} @endsection
@section('header')

@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption ">
                        <i class="fa fa-globe"></i> بيانات عامة
                    </div>
                    <div class="tools"></div>
                </div>

                <div class="portlet-body">


                    <table class="table table-striped table-hover">
                        <tbody>
                        <!-- User Id Field -->
                        <tr>
                            <td>الاسم</td>
                            <td>{{$child->name}}</td>
                        </tr>
                        <tr>
                            <td>ولي الامر</td>
                            <td><a href="{{route('parents.show',$child->parent->id)}}" target="_blank">{{$child->parent->name}}</a></td>
                        </tr>
                        <tr>
                            <td>تاريخ الميلاد</td>
                            <td>{{$child->birthday}}</td>
                        </tr>
                        <tr>
                            <td>العمر</td>
                            <td>{{\Carbon\Carbon::parse($child->birthday)->age}}</td>
                        </tr>
                        <tr>
                            <td>الجنس</td>
                            <td>{{$child->gender=='male'?'ذكر':'أنثي'}}</td>
                        </tr>
                        <tr>
                            <td>الرقم التعريفي</td>
                            <td>{{$child->identify_number}}</td>
                        </tr>
                        <tr>
                            <td>كلمة المرور</td>
                            <td>{{$child->child_password}}</td>
                        </tr>
                        <tr>
                            <td>النقاط</td>
                            <td>{{$child->points}}</td>
                        </tr>


                        <tr>
                            <td> الصورة</td>
                            <td><img src="{{asset($child->image)}}" width="200px"></td>
                        </tr>


                        <tr>
                            <td>عدد المكأفات المستحقة</td>
                            <td>{{count($child->childRewards)}}</td>
                        </tr>
                        <tr>
                            <td>عدد السلوكيات</td>
                            <td>{{$child->childBehaviors->count()}}</td>
                        </tr>

                        <!-- Amount Field -->

                        </tbody>
                    </table>
                </div>


                    <div class="portlet-title">
                        <div class="caption ">
                            <i class="fa fa-address-book"></i> السلوكيات
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered  display  table-hover dt-responsive" width="100%"
                               id="sample_3" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="all  text-center">#</th>
                                <th class="all  text-center">الاسم</th>
                                <th class="all  text-center">النقاط</th>
                                <th class="all  text-center">النوع</th>
                                <th class="min-tablet text-center">الاعدادت</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($child->childBehaviors as $behavior)
                                <tr>
                                    <td class="text-center">{!!$loop->iteration!!}</td>
                                    <td class="text-center">{!!$behavior->behavior->title!!}</td>
                                    <td class="text-center">{!!$behavior->behavior->points!!}</td>
                                    <td class="text-center">{!!$behavior->behavior->type=='good'?'جيد':'سيئ'!!}</td>

                                    {!!Form::open( ['route' => ['child_behaviors.destroy',$behavior->id] ,'id'=>'behavior-delete-form'.$behavior->id, 'method' => 'Delete']) !!}
                                    {!!Form::close() !!}
                                    <td class="text-center" colspan="1">

                                        <div class="margin-bottom-5">
                                            <a onclick="DeleteBehavior({{$behavior->id}})" href="#"
                                               class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-trash"></i> حذف</a>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>



                    </div>

                <div class="portlet-title">
                        <div class="caption ">
                            <i class="fa fa-award"></i> المكأفات
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered  display  table-hover dt-responsive" width="100%"
                               id="sample_3" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="all  text-center">#</th>
                                <th class="all  text-center">الاسم</th>
                                <th class="all  text-center">النقاط</th>
                                <th class="min-tablet text-center">الاعدادت</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($child->childRewards as $award)
                                <tr>
                                    <td class="text-center">{!!$loop->iteration!!}</td>
                                    <td class="text-center">{!!$award->reward->name!!}</td>
                                    <td class="text-center">{!!$award->reward->points!!}</td>

                                    {!!Form::open( ['route' => ['child_rewards.destroy',$award->id] ,'id'=>'reward-delete-form'.$award->id, 'method' => 'Delete']) !!}
                                    {!!Form::close() !!}
                                    <td class="text-center" colspan="1">
                                        <div class="margin-bottom-5">
                                            <a onclick="DeleteReward({{$award->id}})" href="#"
                                               class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                                                <i class="fa fa-trash"></i> حذف</a>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>



                    </div>


            </div>
<button onclick="goBack()" class="btn btn-info btn_1"> للعودة <i class="fa fa-arrow-left" aria-hidden="true"></i>  </button>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>

@endsection
@section('footer')

    <script>

        function DeleteBehavior(id) {
            var item_id=id;

            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا السلوك ؟",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('behavior-delete-form'+item_id).submit();
                }
                else{
                    swal("تم الإلغاء", "حذف السلوك تم الغاؤه", "error");
                }
            });
        };
        function DeleteReward(id) {
            var item_id=id;

            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذه المكأفة ؟",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('reward-delete-form'+item_id).submit();
                }
                else{
                    swal("تم الإلغاء", "حذف المكأفة تم الغاؤه", "error");
                }
            });
        };
    </script>
@endsection


