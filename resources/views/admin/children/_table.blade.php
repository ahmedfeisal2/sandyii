<table class="table table-striped table-bordered  display example table-hover dt-responsive" width="100%"
       id="sample_3" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="all  text-center">#</th>
        <th class="all  text-center">الصورة</th>
        <th class="all  text-center">اسم الطفل</th>
        <th class="all  text-center">ولي الامر</th>
        <th class="all  text-center">الرقم التعريفي</th>
        <th class="all  text-center">تاريخ الميلاد</th>
        <th class="all  text-center">الجنس</th>
        <th class="min-tablet text-center">الاعدادت</th>

    </tr>
    </thead>
    <tbody>
    @foreach($children as $child)
        <tr>
            <td class="text-center">{!!$loop->iteration!!}</td>
            <td><img src="{!! asset($child->image) !!}" width="100px" ></td>
            <td>{!!$child->name!!}</td>
            <td><a href="{{route('parents.show',$child->parent->id)}}" target="_blank">{{$child->parent->name}}</a></td>
            <td>{!!$child->identify_number!!}</td>
            <td>{!!$child->birthday !!}</td>
            <td>{!!$child->gender=='male'?'ذكر':'أنثي'!!}</td>

            {!!Form::open( ['route' => ['children.destroy',$child->id] ,'id'=>'delete-form'.$child->id, 'method' => 'Delete']) !!}
            {!!Form::close() !!}
            <td colspan="1">
                <div class="margin-bottom-5">
                    <a href="{{route('children.set_password',[$child->id])}}"
                       class="btn btn-sm yellow btn-outline filter-submit margin-bottom">
                        <i class="fa fa-user-secret"></i> انشاء كلمة مرور </a>
                </div>
                <div class="margin-bottom-5">
                    <a href="{{route('children.edit',[$child->id])}}"
                       class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil-square"></i> تعديل </a>
                </div>


                <div class="margin-bottom-5">
                    <a onclick="Delete({{$child->id}})" href="#"
                       class="btn btn-sm text-danger btn-outline filter-submit margin-bottom">
                        <i class="fa fa-trash"></i> حذف</a>
                </div>
                <div class="margin-bottom-5">
                    <a href="{{route('children.show',[$child->id])}}"
                       class="btn btn-sm blue btn-outline filter-submit margin-bottom">
                        <i class="fa fa-eye"></i> عرض</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
