<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type',['parent','child']);
            $table->string('name');
            $table->string('mobile')->nullable();
            $table->string('email')->unique();
            $table->text('image')->nullable()->default('public/admin_logo_default.jpg');
            $table->enum('gender',['male','female'])->default('male');
            $table->integer('relation_id')->unsigned()->nullable()->default(null);
            $table->foreign('relation_id')->references('id')->on('relations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string('birthday')->nullable()->default(null);
            $table->string('identify_number')->nullable()->default(null);
            $table->integer('child_password')->nullable()->default(null);
            $table->integer('parent_id')->unsigned()->nullable()->default(null);
            $table->foreign('parent_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
